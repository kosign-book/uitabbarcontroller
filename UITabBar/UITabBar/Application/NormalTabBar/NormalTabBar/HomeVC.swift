//
//  HomeVC.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 15/9/22.
//

import UIKit

class HomeVC: UIViewController {

    //Connect outlet
    @IBOutlet weak var tabBar       : UITabBarItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add badge Value and set color
        self.tabBar?.badgeValue = "100"
        self.tabBar?.badgeColor = .blue
        self.tabBar?.badgeTextAttributes(for: .normal)
    }
}
