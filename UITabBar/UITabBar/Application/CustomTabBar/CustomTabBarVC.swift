//
//  CustomTabBarVC.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 15/9/22.
//

import UIKit

class CustomTabBarVC: UIViewController {

    //MARK: - IBOutlet
    
    //First Tab Bar
    @IBOutlet weak var noticeView   : UIView!
    @IBOutlet weak var noticeImage  : UIImageView!
    @IBOutlet weak var noticLabel   : UILabel!
    @IBOutlet weak var noticeButton : UIButton!
    @IBOutlet weak var noticeVC     : UIView!
    
    //Second Tab Bar
    @IBOutlet weak var findView     : UIView!
    @IBOutlet weak var findImage    : UIImageView!
    @IBOutlet weak var findLabel    : UILabel!
    @IBOutlet weak var findButton   : UIButton!
    @IBOutlet weak var findVC       : UIView!
    
    //Third Tab Bar
    @IBOutlet weak var checkView    : UIView!
    @IBOutlet weak var checkImage   : UIImageView!
    @IBOutlet weak var checkLabel   : UILabel!
    @IBOutlet weak var checkButton  : UIButton!
    @IBOutlet weak var checkVC      : UIView!
    
    //Scroll View
    @IBOutlet weak var indicatorContainer   : UIView!
    @IBOutlet weak var indicatorView        : UIView!
    
    //Constraint
    @IBOutlet weak var scrollBarLeadingConstraint   : NSLayoutConstraint!
    @IBOutlet weak var tabBarViewWidthConstraint    : NSLayoutConstraint!
    @IBOutlet weak var tabBarViewHeightConstraint   : NSLayoutConstraint!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //MARK: - IBAction
    
    @IBAction func didTabBarAction(_ sender: UIButton) {
        self.noticeButton.isSelected    = sender.tag == 0
        self.findButton.isSelected      = sender.tag == 1
        self.checkButton.isSelected     = sender.tag == 2
        self.noticeVC.isHidden          = !(sender.tag == 0)
        self.findVC.isHidden            = !(sender.tag == 1)
        self.checkVC.isHidden           = !(sender.tag == 2)
        switch sender.tag {
        case 0:
            // click on noticeButton
            UIView.animate(withDuration: 0.2, animations: {
                //change the scroll view location to noticeButton by tabBarViewWidthConstraint
                self.scrollBarLeadingConstraint.constant = -self.tabBarViewWidthConstraint.constant
                self.noticeImage.image      = UIImage(named: "home-red-60")
                self.findImage.image        = UIImage(named: "search-60")
                self.checkImage.image       = UIImage(named: "person-60")
                self.noticLabel.textColor   = .red
                self.findLabel.textColor    = .black
                self.checkLabel.textColor   = .black
                self.view.layoutIfNeeded()
            })
        case 1:
            // click on findButton
            UIView.animate(withDuration: 0.2, animations: {
                //change the scroll view location to center or findButton by tabBarViewWidthConstraint
                self.scrollBarLeadingConstraint.constant = 0
                self.noticeImage.image      = UIImage(named: "home-selected-60")
                self.findImage.image        = UIImage(named: "search-red-60")
                self.checkImage.image       = UIImage(named: "person-60")
                self.noticLabel.textColor   = .black
                self.findLabel.textColor    = .red
                self.checkLabel.textColor   = .black
                self.view.layoutIfNeeded()
            })
        case 2:
            // click on checkButton
            UIView.animate(withDuration: 0.2, animations: {
                //change the scroll view location to checkButton by tabBarViewWidthConstraint
                self.scrollBarLeadingConstraint.constant = self.tabBarViewWidthConstraint.constant
                self.noticeImage.image      = UIImage(named: "home-selected-60")
                self.findImage.image        = UIImage(named: "search-60")
                self.checkImage.image       = UIImage(named: "person-red-60")
                self.noticLabel.textColor   = .black
                self.findLabel.textColor    = .black
                self.checkLabel.textColor   = .red
                self.view.layoutIfNeeded()
            })
        default:
            break
        }
    }

    //MARK: - private func
    private func setupView(){
        // returns  the width of the device screen
        self.tabBarViewWidthConstraint.constant     = UIScreen.main.bounds.width / 3
        self.scrollBarLeadingConstraint.constant    = -self.tabBarViewWidthConstraint.constant
        
        self.noticeVC.isHidden  = false
        self.findVC.isHidden    = true
        self.checkVC.isHidden   = true
        
        self.noticLabel.text    = "Home"
        self.findLabel.text     = "Search"
        self.checkLabel.text    = "Profile"
    }

}
