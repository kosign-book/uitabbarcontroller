//
//  UIButton.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 14/9/22.
//

import Foundation

import UIKit

extension UIButton{
    func enable(){
        isUserInteractionEnabled = true
        setTitleColor(UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00), for: .normal)
    }
    func disable(){
        isUserInteractionEnabled = false
        setTitleColor(UIColor(red: 0.77, green: 0.87, blue: 0.96, alpha: 1.00), for: .normal)
    }
    func addDesign(){
        layer.cornerRadius = 15
    }
}
